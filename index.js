const fetch = require('node-fetch')
const blogs = require('./blogs.json')

const allData = []

Promise.all(
  blogs.map(
    blog => fetch(`http://blocs.xtec.cat/eduhack-${blog.codi}/wp-content/plugins/eduhack/ws-eduhack-data.php`)
      .then(response => response.json())
      .then(project => {
        // Normalitza camps de tipus `array`
        ['team_name', 'team_img', 'team_school',
          'facilitador_name', 'facilitador_school',
          'status', 'project_image',
          'tags1_text', 'tags1_img', 'tags1_color',
          'tags2_text', 'tags2_img', 'tags2_color'].forEach(k => project[k] = getArray(project[k]))
        // Identifica logo i estat del projecte
        project.logo = project.project_image[0] || ''
        project.estat = Number(project.status[0] || 0)
        // Ajusta camps complexos        
        project.tags1 = project.tags1_text.map((text, n) => { return { text: text, imatge: project.tags1_img[n] || '', color: project.tags1_color[n] || '' } }) || []
        project.tags2 = project.tags2_text.map((text, n) => { return { text: text, imatge: project.tags2_img[n] || '', color: project.tags2_color[n] || '' } }) || []
        project.equip = project.team_name.map((nom, n) => { return { nom: nom, imatge: project.team_img[n] || '', escola: project.team_school[n] || '' } }) || []
        project.facilitadors = project.facilitador_name.map((nom, n) => { return nom ? { nom: nom, escola: project.facilitador_school[n] || '' } : null }) || []
        return Object.assign({}, blog, project)
      }))
)
  .then(results => console.log(JSON.stringify(results, null, 2)))
  .catch(err => console.log(`ERROR: ${err}`))

function getArray(value) {
  return (!value || !value.length || !value[0]) ? [] : value
}