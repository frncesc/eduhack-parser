
const defaultImage = 'http://projectes.xtec.cat/eduhack/wp-content/uploads/usu882/2017/11/mSchoolsEduHack.png'
const projectsList = 'projectes.json'
const allProjects = []
const allST = []

// Funció a executar a l'inici: llegir projectes.json i omplir el contenidor amb totes les fitxes
window.onload = function () {
  fetch(projectsList)
    .then(response => response.json())
    .then(projects => {

      // Llegeix projectes i construeix targetes
      projects.forEach(prj => {
        allProjects.push({
          prj: prj,
          card: $buildProjectCard(prj)
        })
        if (prj.ST && !allST.includes(prj.ST)) { allST.push(prj.ST) }
      })

      // Omple el contenidor
      fillCardContainer(allProjects)

      // Omple la llista de seus
      allST.forEach(st => $('#sel-st').append($('<option/>', { value: st }).html(st)))
      $('#sel-st').on('change', ev => {
        const filter = ev.target.selectedOptions[0].value
        fillCardContainer(filter === '*' ? allProjects : allProjects.filter(project => project.prj.ST === filter))
      })
    })
    .catch(err => console.error(`ERROR: ${err}`))
}

// Construeix la fitxa del projecte
function $buildProjectCard(project) {
  return $('<a/>', { href: `http://blocs.xtec.cat/eduhack-${project.codi}` }).append(
    $('<div/>', { class: 'project' })
      .append($('<div/>', { class: 'card-body' }).css({ background: `url('${project.logo || defaultImage}') center center / 120% no-repeat` })
        .append($('<h2/>', { class: 'card-title' }).html(project.nom)))
      .append($('<div/>', { class: 'card-footer' })
        .append($('<p/>', { class: 'estat' }).html(`${project.ST} ${estrelletes(project.estat)}`))))
}

// Omple el contenidor de fitxes
function fillCardContainer(projects) {
  const $container = $('#projects')
  $container.empty()
  projects.forEach(prj => $container.append(prj.card))
}

// Representa l'estat amb estrelletes
function estrelletes(estat) {
  let result = ''
  if (estat === 0)
    // Estrelleta buida
    result = '&#9734;'
  else for (var i = 0; i < estat; i++)
    // Estrelletes plenes
    result += '&#9733;'
  return result
}

