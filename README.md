## Processament de dades dels blogs eduHack

### Preparació de l'entorn
- Cal instal·lar el motor `Node.js` (versió 8 o superior) des de: https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
- Un cop instal·lat, cal obrir un terminal, anar a la carpeta del projecte i executar:
```
npm install
```
Amb això s'instal·len les dependències de Node.js requerides pel projecte, que van a parar a la carpeta `node_modules`.

____________________________________________________________

### Captura de les dades dels blogs eduHack
- El fitxer `blogs.json` conté la llista de blogs a parsejar. Per a cada blog cal indicar un codi numèric, la seu, el títol i el "nom curt" del projecte, com en aquest exemple:

```javascript
{
  "id": 105,
  "ST": "BCN",
  "nom": "Xarxes d'entorn social",
  "codi": "ensoc"
}
```

- El fitxer `index.js` realitza la captura de les dades JSON dels blogs referenciats a `blogs.json`, les processa i les fusiona en una única sortida en format JSON. Aquesta sortida és la que ha d'anar al fitxer `projectes.json`, ubicat a la carpeta `web`.

- Per generar el fitxer `web/projectes.json` es pot llençar l'ordre:

```
node index.js > web/projectes.json
```
____________________________________________________________

### Presentació de les targetes dels blogs en HTML
- El directori `web` conté un prototip de web estàtica que presenta les targetes a l'usuari i permet filtrar-les. En principi només caldria actualitzar `web/projectes.json` (amb la instrucció anterior) quan hi hagi canvis en algun blog de projecte.
- El fitxer `web/index.html` llegeix les dades contingudes a `web/projectes.json` i construeix dinàmicament una graella de targetes. De moment és només un esboç.
- Per engegar-lo en un servidor HTTP pla es pot fer, des de la consola:
```
npm run debug
```
- Per parar el servidor: `Ctrl+C`

